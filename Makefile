all:
	eslint *.js content/*/*.js content/*/*.jsm
	rm -f ../tinyjsd-master.xpi
	zip -rD ../tinyjsd-master.xpi * --exclude Makefile
